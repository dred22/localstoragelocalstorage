if(!window.localStorage.getItem('contacts')){
	window.localStorage.setItem('contacts',JSON.stringify({contacts:[]}))
}
//window.localStorage.setItem('contacts',JSON.stringify({contacts:[]}))
var contacts = JSON.parse(window.localStorage.getItem('contacts'));
//var isItShow = -1; //++++++++++++++++++++++++
(function (){
	showContacts();
	//on copie les données du formulaire dans un objet
	var formulaire = document.getElementsByTagName('form')[0];
	formulaire.addEventListener('submit',function(e){
		e.preventDefault();
		var contact = {};
		var ContFromForm = document.getElementsByTagName('input');
		for(var i = 0; i < ContFromForm.length; i++){
			contact[ContFromForm[i].name] = ContFromForm[i].value;
		}
		contact.pays = document.getElementById('pays').value;
		contacts.contacts.push(contact);
		window.localStorage.setItem('contacts',JSON.stringify(contacts));
		showContacts();
	});
	//on ajoute l'evenement pour supression
	var deleteBtns = document.getElementsByClassName('suprimer');
	for (var i = 0; i < deleteBtns.length; i++) {
		addEventDelete(deleteBtns[i]);
	};
	//on ajoute l'evenement pour affichage
	var editBtns = document.getElementsByClassName('afficher');
	for (var i = 0; i < editBtns.length; i++) {
		addEventShow(editBtns[i]);
	}

	function showContacts(){
		var showTable = document.getElementById('showTable');
		showTable.innerHTML = '<caption class="titreTableau"><h2>Liste des contacts</h2></caption><tr><th>Nom</th><th>Suppression</th></tr>';
		for(var i =0; i < contacts.contacts.length; i++){
			addRow(showTable, contacts.contacts[i].prenom, i);
		}
	}

	function addRow(elmt, value, index){
	    var tr = document.createElement('tr');
	    elmt.appendChild(tr);
	    var td = document.createElement('td');
	  	td.classList.add('afficher');
	    var tdBtn = document.createElement('td');
	    tdBtn.classList.add('suprimer');
	    addEventDelete(tdBtn);
		td.dataset.index = index;
	    tdBtn.dataset.index = index;
	    tr.appendChild(td);
	    tr.appendChild(tdBtn);
	    var tdText = document.createTextNode(value);
	    td.appendChild(tdText);
	    tdBtn.appendChild(document.createTextNode('X'));
	}
	function addEventDelete(elem){
		elem.addEventListener('click', function(){
			var tempTable = [];
			for (var i = 0; i < contacts.contacts.length; i++) {
				if (i != this.dataset.index){
					tempTable.push(contacts.contacts[i]);
				}
			};
			contacts.contacts = tempTable;
			window.localStorage.setItem('contacts',JSON.stringify(contacts));
			showContacts();
		});
	}
	function addEventShow(elem){
		elem.addEventListener('click', function(){
			var contact = contacts.contacts[this.dataset.index];
			for (var i in contact) {
				 if (contact.hasOwnProperty(i)){
				 	//console.log(contact[i], i);
				 	for (var ind = 0; ind < formulaire.length; ind++) {
				 		//console.log(formulaire[ind].name, i);
						if (formulaire[ind].name == i){
							formulaire[ind].value = contact[i];

						}
					};
				 }
			};
			//isItShow = index;//++++++++++++++++++++++++
		});
	}

})()
